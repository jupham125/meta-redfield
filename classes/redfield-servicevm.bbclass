do_install[depends] += "${BPN}-image:do_image_complete"

DEST_IMAGE_PATH ?= "/images/"
DEST_FILE_NAME ?= "${BPN}.raw"

SRC_URI = " \
    file://${BPN}.yaml \
"

do_install() {
    mkdir -p ${D}/images
    mkdir -p ${D}/${sysconfdir}/installer.d

    install -m 0644 ${DEPLOY_DIR_IMAGE}/${BPN}-image-${MACHINE}.ext4 ${D}${DEST_IMAGE_PATH}${DEST_FILE_NAME}
    install -m 0644 ${WORKDIR}/${BPN}.yaml ${D}/${sysconfdir}/installer.d/${BPN}.yaml
}

FILES_${PN} = " \
    ${DEST_IMAGE_PATH}${DEST_FILE_NAME} \
    ${sysconfdir}/installer.d/${BPN}.yaml \
"
