DESCRIPTION = "Hide Activities Button"
LICENSE = "Unlicense"
DEPENDS = " \
    gnome-shell \
    gsettings-desktop-schemas \
    glib-2.0-native \
"

LIC_FILES_CHKSUM = "file://LICENSE;md5=7246f848faa4e9c9fc0ea91122d6e680"

SRCREV = "657498640a163ab8960c8e31dcb768ddb32b22d6"

SRC_URI = "git://github.com/shayelkin/gnome-hide-activities;protocol=http;branch=master"

S = "${WORKDIR}/git"

FILES_${PN} += "${datadir}"

do_install() {
    MODULE="Hide_Activities@shay.shayel.org"
    DESTDIR="${D}/${datadir}/gnome-shell/extensions/${MODULE}"

    install -d ${DESTDIR}
    install -m 0644 ${S}/${MODULE}/* ${DESTDIR}/
}
