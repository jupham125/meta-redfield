DESCRIPTION = "Wifimenu extension for Gnome Shell"
LICENSE = "Apache-2.0"
DEPENDS = " \
            gnome-shell \
            gsettings-desktop-schemas \
	    glib-2.0-native \
          "

LIC_FILES_CHKSUM = "file://../LICENSE;md5=38f7323dc81034e5b69acc12001d6eb1"

SRC_URI = " \
    git://${REDFIELD_SOURCES_URL}/gnome-extensions.git;protocol=${REDFIELD_SOURCES_PROTOCOL};branch=${REDFIELD_BRANCH}; \
"

SRCREV="${AUTOREV}"

S = "${WORKDIR}/git/wifimenu@redfield"

FILES_${PN} += "${datadir}"

export DESTDIR="${D}/${datadir}/gnome-shell/extensions/wifimenu@redfield"

do_install() {
	oe_runmake install
}
