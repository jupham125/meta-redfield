DESCRIPTION = "Dash to Dock for Gnome Shell"
LICENSE = "GPLv2"
DEPENDS = " \
            gnome-shell \
            gsettings-desktop-schemas \
	    glib-2.0-native \
          "

LIC_FILES_CHKSUM = "file://COPYING;md5=d791728a073bc009b4ffaf00b7599855"

SRC_URI = "git://github.com/micheleg/dash-to-dock.git;protocol=http;branch=master;tag=extensions.gnome.org-v64 \
           "

S = "${WORKDIR}/git"

inherit pkgconfig gettext

FILES_${PN} += "/usr/share/gnome-shell"
FILES_${PN} += "/usr/share/glib-2.0"
FILES_${PN} += "/usr/share/gnome-shell/extensions"
FILES_${PN} += "/usr/share/glib-2.0/schemas"
FILES_${PN} += "/usr/share/glib-2.0/schemas/org.gnome.shell.extensions.dash-to-dock.gschema.xml"

export DESTDIR="${D}"

do_compile() {
    oe_runmake all
}

do_install() {
    oe_runmake install
}