FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://custom.conf \
    file://race-fix.patch \
"

do_install_append() {
    install -m 0755 ${WORKDIR}/custom.conf ${D}${sysconfdir}/gdm/custom.conf
}
