#!/bin/sh
#
# Copyright (c) 2018 Assured Information Security, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# bridge name is read from xenstore and a prefix (${backend_name}-) is added.
# vifD.n-emu <-> ${bridgename} <-> eth0
# 
# This script is not responsible for attaching eth0 to bridge

# Exit if the vif is not qemu emulated.
EMUD=$(echo $1 | awk '{print match($0,"emu")}')
if [[ $EMUD -eq 0 || "x$2" != "xtun" ]]; then
   exit 0
fi

# Get vifID from interface name.
VAR=${1##*.}
VIFID=${VAR%-emu}

# Get domID from interface name.
VAR=$(echo $1 | cut -f 1 -d '.')
DOMID=$(echo $VAR | awk -F 'vif' '{print $NF}')

# Read bridge name from xenstore.
# Note: When udev is triggered xenstore node is not updated yet, hence watch.
bridgename=$(xenstore-read /libxl/${DOMID}/device/vif/${VIFID}/bridge)
if [ -z ${bridgename} ]; then
    xenstore-watch -n 2 /libxl/${DOMID}/device/vif/${VIFID}/bridge
    bridgename=$(xenstore-read /libxl/${DOMID}/device/vif/${VIFID}/bridge)
fi

# Get backend domid from xenstore and hence its name, prepend this to bridgename.
BACKENDID=$(xenstore-read /local/domain/${DOMID}/device/vif/${VIFID}/backend-id)
BACKENDNAME=$(xl domname ${BACKENDID})
dom0bridge="${bridgename}-dom${BACKENDID}"

# Acquire lock (flock) before checking/creating bridge.
(

flock 100
if [ ! -e "/sys/class/net/${dom0bridge}" ];then
    brctl addbr ${dom0bridge}
    ip link set ${dom0bridge} up
fi

# Add bridge port to the backend
xl network-attach 0 backend=${BACKENDNAME} bridge=${bridgename}

# Find out the name of the interface just created, and add it to
# to the new bridge in dom0
dom0VIFID=$(xl network-list 0 | tail -1 | awk -F' ' '{print $1}')
dom0iface=$(ls -1 /sys/devices/vif-${dom0VIFID}/net/)

ip link set $dom0iface up

brctl addif ${dom0bridge} $1
brctl addif ${dom0bridge} ${dom0iface}

ip link set $dom0bridge up

# NOTE: bridge name is being written to lock file because it cannot be
#       contructed back when vif is removed to delete the bridge.
#       Reconstruction of bridge name involves reading xenstore node
#       associated with vif and is no longer present when vif is removed.
#
#       The vif ID for the interface in dom0 (e.g. eth0) is also written
#       to the lock file for purposes of tearing down the correct interfaces.

echo "$1:$dom0bridge:$dom0VIFID" >&100

)100>>/var/lock/bridge-lock

# Bring the interface up.
ip link set $1 up
