SUMMARY = "Gotron builder"
HOMEPAGE = "https://github.com/Equanox/gotron"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

PROVIDES = "\
    ${PN} \
"

inherit native go

GO_IMPORT = "github.com/Equanox/gotron"
SRC_URI = "git://github.com/Equanox/gotron;dest-suffix=${PN}-${PV}/src/${GO_IMPORT}"
SRCREV = "ce7e0301b8639ffe404124892b4d7f838f5c8261"

do_compile_prepend() {
    cd src/${GO_IMPORT} && GOMODULE111=on go get ./...
}

do_install_append() {
    rm -f ${D}${bindir}/example
}

BBCLASSEXTEND = "native"

GOBUILDMODE = 'exe'

FILES_${PN} = "${bindir}/gotron-builder"
FILES_${PN}-native = "${bindir}/gotron-builder"
