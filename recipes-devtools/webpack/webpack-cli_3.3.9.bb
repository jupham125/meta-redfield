SUMMARY = "Webpack CLI"
HOMEPAGE = "https://webpack.js.org/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

PROVIDES = "\
    ${PN} \
"

DEPENDS = "\
    webpack \
"

SRC_URI = "npm://registry.npmjs.org;name=webpack-cli;version=${PV}"

inherit native npm

# Must be set after inherit npm since that itself sets S
S = "${WORKDIR}/npmpkg"

BBCLASSEXTEND = "native"

do_install_append() {
    rm -f ${D}${libdir}/node
}

FILES_${PN} = "${bindir}/webpack-cli"
FILES_${PN}-native = "${bindir}/webpack-cli"

