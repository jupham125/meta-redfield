FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

DEPENDS_append_class-target = " linux-libc-headers "

SRC_URI_append_class-target = " \
        file://0001-Revert-block-avoid-recursive-block_status-call-if-po.patch \
        file://1000-redfield-improve-focus.patch \
        file://2000-sdl-set-wmclass.patch \
"
