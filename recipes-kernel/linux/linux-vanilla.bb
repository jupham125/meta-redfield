DEPENDS += "${@bb.utils.contains('ARCH', 'x86', 'elfutils-native', '', d)}"
DEPENDS += "openssl-native util-linux-native"

COMPATIBLE_MACHINE = "qemuarm|qemuarm64|qemux86|qemuppc|qemumips|qemumips64|qemux86-64|intel-corei7"
KMACHINE ?= "intel-corei7-64"
KBRANCH ?= "linux-5.4.y"
LINUX_VERSION ?= "5.4.2"
SRCREV ?= "5f8bc2bb0e0f456e2217bbd1caac2acf211431c9"

SRC_URI = " \
           git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=git;branch=${KBRANCH} \
           file://defconfig \
           file://redfield.scc \
           file://xen.cfg \
           file://usb-override.cfg \
           file://0001-dont-init-pmc-core-in-pv-guests.patch \
           file://openxt-netfront-support-backend-relocate.patch \
"

PV = "${LINUX_VERSION}+git${SRCPV}"

inherit kernel kernel-yocto

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"
LICENSE="GPLv2"

