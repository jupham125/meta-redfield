SUMMARY = "Desirable packages for redfield usb support"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} = " \
    go-usbctl \
    usbutils \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-gui', 'usbmenu', '', d)} \
"
