SUMMARY = "Redfield usbctl"
HOMEPAGE = "http://www.gitlab.com/redfield/usbctl"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=38f7323dc81034e5b69acc12001d6eb1"

DEPENDS = " \
    grpc \
    libusb \
"

inherit go pkgconfig systemd

GO_IMPORT = "gitlab.com/redfield/usbctl"
SRC_URI = " \
    git://${REDFIELD_SOURCES_URL}/usbctl.git;protocol=${REDFIELD_SOURCES_PROTOCOL};branch=${REDFIELD_BRANCH};destsuffix=${PN}-${PV}/src/${GO_IMPORT} \
    file://usbctl-service.service \
"

SRCREV = "${AUTOREV}"

GOBUILDMODE = 'exe'

do_install_append() {
    DESTDIR=${D} make -C src/${GO_IMPORT} install
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/usbctl-service.service ${D}${systemd_system_unitdir}
}

SYSTEMD_SERVICE_${PN} = "usbctl-service.service"

RDEPENDS_${PN}-dev = "bash make"
