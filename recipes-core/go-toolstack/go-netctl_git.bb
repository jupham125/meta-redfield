SUMMARY = "Redfield netctl"
HOMEPAGE = "http://www.gitlab.com/redfield/netctl"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=38f7323dc81034e5b69acc12001d6eb1"

DEPENDS = " \
    grpc \
"

inherit go pkgconfig systemd

GO_IMPORT = "gitlab.com/redfield/netctl"
SRC_URI = " \
    git://${REDFIELD_SOURCES_URL}/netctl.git;protocol=${REDFIELD_SOURCES_PROTOCOL};branch=${REDFIELD_BRANCH};destsuffix=${PN}-${PV}/src/${GO_IMPORT} \
    file://netctl-back.service \
"

SRCREV = "${AUTOREV}"

GO_LINKSHARED = ""
GOBUILDMODE = 'exe'

do_install_append() {
    DESTDIR=${D} make -C src/${GO_IMPORT} install
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/netctl-back.service ${D}${systemd_system_unitdir}
}

SYSTEMD_SERVICE_${PN} = "netctl-back.service"
SYSTEMD_AUTO_ENABLE = "disable"

RDEPENDS_${PN} = "bash bash-completion"
RDEPENDS_${PN}-dev = "bash make"
