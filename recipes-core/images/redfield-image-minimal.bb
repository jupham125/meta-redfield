DESCRIPTION = "Redfield Minimal Image"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit redfield-image

IMAGE_FSTYPES = "ext4"

IMAGE_INSTALL += " \
    ${@bb.utils.contains('MACHINE_FEATURES', 'pci', 'pciutils', '', d)} \
    kernel-image-bzimage \
    kernel-modules \
"

IMAGE_ROOTFS_EXTRA_SPACE += " + 32768 "

