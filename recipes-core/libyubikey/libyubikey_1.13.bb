SUMMARY = "Yubikey Library"

HOMEPAGE = "https://www.yubico.com/products/services-software/download/yubikey-personalization-tools"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=82e429e9fd33c7ce9f12030aa107402d"

SRC_URI = " \
    https://developers.yubico.com/yubico-c/Releases/${PN}-${PV}.tar.gz \
"

inherit autotools pkgconfig

SRC_URI[md5sum] = "6e84fc1914ab5b609319945c18d45835"
SRC_URI[sha256sum] = "04edd0eb09cb665a05d808c58e1985f25bb7c5254d2849f36a0658ffc51c3401"

