SUMMARY = "initramfs-framework module for Redfield Text mode display banner"
DESCRIPTION = "This module will decrypt and mount the main Redfield logical \
volume."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r0"

inherit allarch

FILESEXTRAPATHS_prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://banner-redfield"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d
    install -m 0755 ${WORKDIR}/banner-redfield ${D}/init.d/82-banner-redfield
}

FILES_${PN} = "/init.d/82-banner-redfield"

RDEPENDS_${PN} = " \
    initramfs-framework-base \
"

