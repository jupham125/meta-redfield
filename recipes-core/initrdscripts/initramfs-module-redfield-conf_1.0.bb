SUMMARY = "Shared configuration for redfield initramfs-framework modules.."
DESCRIPTION = "Provide a shared configuration for the initramfs modules used in \
Redfield's intramfs."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r0"

inherit allarch

FILESEXTRAPATHS_prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://conf-redfield"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d
    install -m 0755 ${WORKDIR}/conf-redfield ${D}/init.d/00-conf-redfield
}

FILES_${PN} = "/init.d/00-conf-redfield"

RDEPENDS_${PN} = " \
    initramfs-framework-base \
"
