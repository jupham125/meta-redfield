SUMMARY = "initramfs-framework module for Redfield LVM handling."
DESCRIPTION = "This module will search, decrypt and mount the main Redfield \
logical volume."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r0"

inherit allarch

FILESEXTRAPATHS_prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://lvm-redfield"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d
    install -m 0755 ${WORKDIR}/lvm-redfield ${D}/init.d/80-lvm-redfield
}

FILES_${PN} = "/init.d/80-lvm-redfield"

RDEPENDS_${PN} = " \
    initramfs-framework-base \
    initramfs-module-redfield-conf \
    coreutils \
    lvm2 \
    udev \
"

