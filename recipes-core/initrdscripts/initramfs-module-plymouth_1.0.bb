SUMMARY = "initramfs-framework module for plymouth."
DESCRIPTION = "This module will start the plymouthd."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r0"

inherit allarch

FILESEXTRAPATHS_prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://plymouth"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d
    install -m 0755 ${WORKDIR}/plymouth ${D}/init.d/99-plymouth
}

FILES_${PN} = "/init.d/99-plymouth"

RDEPENDS_${PN} = " \
    initramfs-framework-base \
    plymouth \
"
