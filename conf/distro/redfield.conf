DISTRO_VERSION = "v0"

#Images built can have to modes:
# 'debug': empty rootpassword, strace included
# 'release' no root password, no strace and gdb by default
DISTRO_TYPE ?= "debug"
#DISTRO_TYPE = "release"

# We want to ship extra debug utils in the rootfs when doing a debug build
DEBUG_APPS ?= ""
DEBUG_APPS += '${@base_conditional("DISTRO_TYPE", "release", "", "strace procps",d)}'

require conf/distro/include/yocto-uninative.inc

INHERIT += "uninative"

DISTRO_FEATURES_append = " \
    efi \
    ldconfig \
    opengl \
    pam \
    polkit \
    redfield-dev \
    redfield-audio \
    redfield-gui \
    redfield-network \
    redfield-storage \
    redfield-usb \
    systemd \
    virtualization \
    vulkan \
    wayland \
    x11 \
    xattr \
    xen \
"

DISTRO_FEATURES_remove = "sysvinit"

VIRTUAL-RUNTIME_init_manager = "systemd"

PACKAGECONFIG_pn-systemd = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'xkbcommon', '', d)} \
    ${@bb.utils.filter('DISTRO_FEATURES', 'efi ldconfig pam selinux usrmerge', d)} \
    binfmt \
    cryptsetup \
    firstboot \
    hibernate \
    hostnamed \
    ima \
    iptc \
    kmod \
    localed \
    logind \
    machined \
    myhostname \
    networkd \
    nss \
    polkit \
    quotacheck \
    randomseed \
    resolved \
    serial-getty-generator \
    smack \
    sysusers \
    timedated \
    timesyncd \
    utmp \
    vconsole \
    xz \
"

PACKAGECONFIG_pn-qemu = " \
    aio \
    alsa \
    bluez \
    fdt \
    glx \
    gtk+ \
    libusb \
    pulseaudio \
    sdl \
    spice \
    usb-redir \
    virglrenderer \
    xen \
"

PACKAGECONFIG_append_pn-pulseaudio = " \
    autospawn-for-root \
"

PREFERRED_PROVIDER_virtual/kernel = "linux-vanilla"

################################
# MACHINE FEATURES
################################

# No legacy boot support currently
MACHINE_FEATURES_remove = "pcbios"

################################
# HACKS AND UPSTREAM WORKAROUNDS
################################

# libdevmapper relies on udev, which is sytemd
# we configure systemd to rely on cryptsetup which relies on libdevmapper
# UPSTREAM-STATUS: https://patchwork.openembedded.org/patch/140207/
PACKAGECONFIG_pn-lvm2 = "odirect dmeventd lvmetad thin-provisioning-tools \
    ${@bb.utils.filter('DISTRO_FEATURES', 'selinux', d)} \
"
PACKAGECONFIG_pn-libdevmapper = "odirect dmeventd lvmetad thin-provisioning-tools \
    ${@bb.utils.filter('DISTRO_FEATURES', 'selinux', d)} \
"
PACKAGECONFIG_remove_pn-evolution-data-server="introspection"
# qemu requires gettext
# UPSTREAM-STATUS: https://patchwork.openembedded.org/patch/150703/
DEPENDS_append_pn-qemu = " gettext-native"
DEPENDS_append_pn-qemu-native = " gettext-native"

PREFERRED_VERSION_xen = "4.13.0"
XEN_GIT_pn-xen = "xenbits.xen.org/xen.git"

#REDFIELD_INSTALLER_SOURCES_URL ?= ""
#REDFIELD_INSTALLER_BRANCH ?= ""

