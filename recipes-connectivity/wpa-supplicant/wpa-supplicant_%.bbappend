FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://wpa_supplicant.service \
    file://wpa_supplicant.sysctl \
"

SYSTEMD_AUTO_ENABLE = "enable"

do_install_append() {
    install -d -m 755 ${D}${systemd_system_unitdir}
    install -d -m 755 ${D}${sysconfdir}/sysconfig
    install -p -m 644 ${WORKDIR}/wpa_supplicant.service ${D}${systemd_system_unitdir}
    install -p -m 644 ${WORKDIR}/wpa_supplicant.sysctl ${D}${sysconfdir}/sysconfig/wpa_supplicant
}

FILES_${PN} += "${systemd_system_unitdir}"
FILES_${PN} += "${sysconfdir}/sysconfig"
